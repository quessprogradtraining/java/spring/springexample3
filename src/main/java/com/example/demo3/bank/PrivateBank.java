package com.example.demo3.bank;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class PrivateBank {
    private int age;
    private  int amount;


    public PrivateBank() {
    }

    public PrivateBank(int age, int amount) {
        this.age = age;
        this.amount = amount;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    public void inputs(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter The Age");
        age=scanner.nextInt();
        System.out.println("Enter The Amount");
        amount=scanner.nextInt();
    }
    public void calculateInterest(){
        if(age>60) {


            double interest = amount+amount*0.08;
            System.out.println(interest);

        }
        else{
            double interest=amount+amount*0.04;
            System.out.println(interest);
        }
    }
}
