package com.example.demo3;

import com.example.demo3.bank.PrivateBank;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Demo3Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context= SpringApplication.run(Demo3Application.class, args);
		PrivateBank privateBank=context.getBean(PrivateBank.class);
		privateBank.inputs();
		privateBank.calculateInterest();

	}

}
